package com.multipro.dans.testapi.service;

import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.springframework.stereotype.Service;

@Service
public class EncryptService 
{

    public String encrypt(String strClearText, String strKey) throws Exception {
        String strData = "";
        try {
            SecretKeySpec skeyspec = new SecretKeySpec(strKey.getBytes(), "Blowfish");
            Cipher cipher = Cipher.getInstance("Blowfish");
            cipher.init(Cipher.ENCRYPT_MODE, skeyspec);
            byte[] encrypted = cipher.doFinal(strClearText.getBytes());
            strData = Base64.getEncoder().encodeToString(encrypted);
 
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(e);
        }
        return strData;
    }
 
    public String decrypt(String strEncrypted, String strKey) throws Exception {
        String strData = "";
 
        try {
            SecretKeySpec skeyspec = new SecretKeySpec(strKey.getBytes(), "Blowfish");
            Cipher cipher = Cipher.getInstance("Blowfish");
            cipher.init(Cipher.DECRYPT_MODE, skeyspec);
            System.out.println("string encrypted : " + strEncrypted);
            System.out.println("string key : " + strKey);
            byte[] decrypted = cipher.doFinal(Base64.getDecoder().decode(strEncrypted));
            strData = new String(decrypted);
 
        } catch (Exception e) {
            System.out.println("error : " + e.toString());
            e.printStackTrace();
            throw new Exception(e);
        }
        return strData;
    }
}
