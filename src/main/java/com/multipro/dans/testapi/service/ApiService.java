package com.multipro.dans.testapi.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.multipro.dans.testapi.model.User;
import com.multipro.dans.testapi.repository.UserRepository;

import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.supercsv.io.CsvListWriter;
import org.supercsv.io.ICsvListWriter;
import org.supercsv.prefs.CsvPreference;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class ApiService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private EncryptService encryptService;

    private static String url = "https://jobs.github.com/positions";

    public Flux<Document> listApi() {
        return WebClient.create().get().uri(url + ".json").retrieve().bodyToFlux(Document.class);
    }

    public Mono<Document> detailJob(String id) {
        return WebClient.create().get().uri(url + "/" + id + ".json").retrieve().bodyToMono(Document.class);
    }

    public ByteArrayInputStream csv() throws IOException
    {
        Flux<Document> d = this.listApi();
        d.subscribe();
        Iterable<Document> itd = d.toIterable();
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        ICsvListWriter writer = new CsvListWriter(new OutputStreamWriter(out), CsvPreference.STANDARD_PREFERENCE);
        List<String> listWriter = Arrays.asList("id","type","url","created at","company","company url","location","title","description","how to apply","company logo");
        writer.write(listWriter);
        for (Document docs : itd)
        {
            listWriter = new ArrayList<>();
            for (String s : docs.keySet())
            {
                listWriter.add(docs.getString(s));
            }
            writer.write(listWriter);
        }
        writer.close();
        return new ByteArrayInputStream(out.toByteArray());
    }

    public User login(Document payload) throws Exception
    {
        short enabled = 1;
        User u = userRepository.findByUserNameAndEnabled(payload.getString("username"), enabled );
        if (u != null)
        {
            if (payload.getString("username").equals(u.getUserName()) && payload.getString("password").equals(this.encryptService.decrypt(u.getPassword(), "dans")))
            {
                return u;
            } 
            return null;
        }
        return null;
    }


}