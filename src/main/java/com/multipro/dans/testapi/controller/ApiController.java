package com.multipro.dans.testapi.controller;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import com.multipro.dans.testapi.model.User;
import com.multipro.dans.testapi.service.ApiService;
import com.multipro.dans.testapi.service.EncryptService;

import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import org.springframework.web.bind.annotation.RequestMethod;

@RestController
public class ApiController
{

    @Autowired
    private ApiService service;

    @Autowired
    private EncryptService encryptService;

    @RequestMapping(value = "/api/login", method = RequestMethod.POST)
    public Mono<ResponseEntity<Object>> login (@RequestBody String payload) throws Exception
    {
        Document payloadDocs = Document.parse(payload);
        if (payloadDocs.containsKey("username") && payloadDocs.containsKey("password"))
        {
            User u = this.service.login(payloadDocs);
            if (u != null)
            {
                return Mono.just(ResponseEntity.ok().body(this.service.login(payloadDocs)));
            }
            return Mono.just(ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(new Document().append("message", "credential invalid")));
        } else 
        {
            return Mono.just(ResponseEntity.badRequest().body(new Document().append("message", "username or password is missing")));
        }
    }

    @RequestMapping(value = "/encrypt/{value}", method = RequestMethod.GET)
    public Mono<Document> encrypt (@PathVariable String value) throws Exception
    {
        return Mono.just(new Document().append("result", this.encryptService.encrypt(value, "dans")));
    }


    @RequestMapping(value = "/jobs", method = RequestMethod.GET)
    public Flux<Document> getJobs ()
    {
        return this.service.listApi();
    }

    @RequestMapping(value = "/jobs/{id}", method = RequestMethod.GET)
    public Mono<Document> getJobs (@PathVariable final String id)
    {
        return this.service.detailJob(id);
    }

    @RequestMapping(value = "/jobs/csv", method = RequestMethod.GET)
    public Mono<ResponseEntity<InputStreamResource>> getCSV () throws IOException
    {
        ByteArrayInputStream csv = this.service.csv();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "text/csv");
        String filename = "jobs.csv";
        headers.add("Content-Disposition", "attachment; filename=\""+filename+"\"");
        return Mono.just(ResponseEntity.ok().headers(headers).body(new InputStreamResource(csv)));
    }

    
}