### Description 

this a repository for code challange Dans Multipro. it a spring boot code with 4 apis.

- login 

    assumption is only for login needs, not to create a security system
    ```
    endpoint : /api/login
    method : POST
    body : 
    {
        "username":"admin",
        "password":"password"
    }
    ```

- jobs

    ```
    endpoint : /jobs
    method : GET
    ```

- job detail

    ```
    endpoint : /jobs/{id}
    method : GET
    ```

- job csv

    ```
    endpoint : /jobs/csv
    method : GET
    ```

to see database you can access : `http://localhost:8080/h2/`

and set the field with specification below

- JDBC URL : `jdbc:h2:file:~/spring/testapi`
- username : `sa`
- Driver Class : `org.h2.Driver`

to encrypt password you can use api `localhost:8080/encrypt/{string to encrypt}`